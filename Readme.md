###ABChosting test 

# To run the app on your local machine

# make sure you have Mysql, php, apache2 0r nginx on your machine.

1. git clone the repo

    e.g git clone https://gitlab.com/josephdilas/abchostingecommerce

2. open your terminal & cd into the directory

3. configure your db by going into db.php file
    e.g $servername = "localhost";
        $username = "root";
        $password = "password";
        $db = "ABChostingstore";

4. then run 'php -S localhost:8000/index.php

5. you can then open your browser to start using your ecommerce.