function showRestaurantData(url)
			{
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function () {
					if (this.readyState == 4 && this.status == 200)
					{
						document.getElementById("cart_list").innerHTML = this.responseText;
					}
				};
				xhttp.open("GET", url, true);
				xhttp.send();

			} 

			function mouseOverRating(restaurantId, rating) {

				resetRatingStars(restaurantId)

				for (var i = 1; i <= rating; i++)
				{
					var ratingId = restaurantId + "_" + i;
					document.getElementById(ratingId).style.color = "#ff6e00";

				}
			}

			function resetRatingStars(restaurantId)
			{
				for (var i = 1; i <= 5; i++)
				{
					var ratingId = restaurantId + "_" + i;
					document.getElementById(ratingId).style.color = "#9E9E9E";
				}
			}

		function mouseOutRating(restaurantId, userRating) {
			var ratingId;
			if(userRating !=0) {
					for (var i = 1; i <= userRating; i++) {
							ratingId = restaurantId + "_" + i;
						document.getElementById(ratingId).style.color = "#ff6e00";
					}
			}
			if(userRating <= 5) {
					for (var i = (userRating+1); i <= 5; i++) {
						ratingId = restaurantId + "_" + i;
					document.getElementById(ratingId).style.color = "#9E9E9E";
				}
			}
			}

			function addRating (restaurantId, ratingValue) {
					var xhttp = new XMLHttpRequest();

					xhttp.onreadystatechange = function ()
					{
						if (this.readyState == 4 && this.status == 200) {

							showRestaurantData('cart.php');
							
							if(this.responseText != "success") {
								alert(this.responseText);
							}
						}
					};

					xhttp.open("POST", "insertRating.php", true);
					xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					var parameters = "index=" + ratingValue + "&p_id=" + restaurantId;
					xhttp.send(parameters);
			}